#!/bin/bash
all_image_files=(
roles/download/defaults/main.yml
roles/kubernetes-apps/ansible/defaults/main.yml

)

for file in ${all_image_files[@]} ; do
    sed -i 's/gcr.io/registry.umarkcloud.com:8088/g' $file
    sed -i 's/quay.io/registry.umarkcloud.com:8088/g' $file
done

